if (isset($_REQUEST['submitMessage'])) {

	if (empty($_REQUEST['g-recaptcha-response'])) {
			sleep(25);
			die('not human');
	}
	$opts = array('http' =>
				array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => http_build_query( $post =
						array(
							'secret' => 'XXXXXXX-the-secret-key-XXXXXXXXXX',
							'response' => $_REQUEST['g-recaptcha-response'],
						)
					),
				),
			);
	if (in_array(ini_get('allow_url_fopen'), array('On', 'on', '1'))) {
		$stream = stream_context_create($opts);
		$captcha = @json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $stream), true);
	}
	elseif (function_exists('curl_init')) {
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $post,
			)
		);
		$captcha = @json_decode(curl_exec($curl),true);
		curl_close($curl);
	}
	else {
		// buggy hoster !
		$captcha = array('error' => 'buggy hoster');
	}
	if (empty($captcha['success'])){

		if(!empty($_SERVER['HTTP_REFERER'])) {
			sleep(5);
			die('<p>Captcha Invalide</p><a href="'.$_SERVER['HTTP_REFERER'].'"><button>Retour au formulaire</button></a>');
		}
		sleep(25);
		die('not human');
	}
}