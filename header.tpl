<!-- Copyright (c)2017-2018 (d)oekia Enter-Solutions GPL -->
<!-- Google ReCaptcha on contact form -->
<script>
(function(){
	var googlecaptchasitekey = 'XXXXXXX-the-public-site-key-XXXXXXXXX';
	var trigger = function(){
		setTimeout(function(){
			$('div.g-recaptcha').remove();
			var $forms = $('form.contact-form-box,form#sendOrderMessage');
			if ($forms.length > 0){
				var captcha = $('<div class="g-recaptcha" data-sitekey="'+ googlecaptchasitekey + '">');
				var $submit = $forms.find('#submitMessage,.button[name=submitMessage]');
				$submit.before(captcha);
				$submit.click(function(event){
					if ($forms.find('#g-recaptcha-response').val().length == 0) {
						event.preventDefault();
						event.stopPropagation();
						return false;
					}
				});
				try {
						window.grecaptcha.render(captcha[0]);
				} catch(e){};
			}
		},1000);
	};
	$(document).ready(trigger);
	$(document).bind('ajaxComplete', trigger);
	})();
</script>
{if !isset($language_code)}{assign var="language_code" value=$lang_iso}{/if}
<script src="https://www.google.com/recaptcha/api.js?hl={$language_code}"></script>
<script>
	if ( window.history.replaceState ) {
		window.history.replaceState( null, null, window.location.href );
	}
</script>